package controller

import (
	"encoding/json"
	"github.com/gorilla/mux"
	"gitlab.com/vjopensrc/webserver_demo/model"
	"gitlab.com/vjopensrc/webserver_demo/utility"
	"net/http"
	"strconv"
)

func TicketCreateHandler(w http.ResponseWriter, r *http.Request, ctx *model.Context) (err error) {
	// ticket := model.Ticket{}
	// r.ParseForm()
	// ticket.Subject = r.Form.Get("subject")
	// ticket.Description = r.FormValue("description")
	// log.Println("subject ::::", r.Form.Get("subject"))

	ticket := ticketDecoder(r)
	ticket.Create(ctx)
	response := model.MakeResponse(utility.ResponseSuccess, "ticket created", ticket)
	json.NewEncoder(w).Encode(response)
	return nil
}

func TicketListHandler(w http.ResponseWriter, r *http.Request, ctx *model.Context) (err error) {
	tickets := model.Tickets(ctx)
	response := model.MakeResponse(utility.ResponseSuccess, "ticketlist", tickets)
	json.NewEncoder(w).Encode(response)
	return nil
}

func TicketDetailHandler(w http.ResponseWriter, r *http.Request, ctx *model.Context) (err error) {
	ticket := model.TicketDetail(ctx, GetTicketId(r))
	notes := model.Notes(ctx, ticket.Id)
	response := model.MakeResponse(utility.ResponseSuccess, "ticketdetail", ticket, notes)
	json.NewEncoder(w).Encode(response)
	return nil
}

func GetTicketId(r *http.Request) int64 {
	params := mux.Vars(r)
	ticketid := params["Ticketid"]
	if ticketid == "" {
		ticketid = r.FormValue("Ticketid")
	}
	i, err := strconv.ParseInt(ticketid, 10, 64)
	if err != nil {
		return 0
	}
	return i
}

func ticketDecoder(r *http.Request) model.Ticket {
	// body, _ := ioutil.ReadAll(r.Body)
	// log.Println("req body ::: ", string(body))
	r.ParseForm()
	decoder := json.NewDecoder(r.Body)
	var t model.Ticket
	err := decoder.Decode(&t)
	if err != nil {
		panic(err)
	}
	return t
}
