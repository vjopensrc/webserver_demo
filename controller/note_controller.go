package controller

import (
	"encoding/json"
	"gitlab.com/vjopensrc/webserver_demo/model"
	"gitlab.com/vjopensrc/webserver_demo/utility"
	"net/http"
)

func NoteCreateHandler(w http.ResponseWriter, r *http.Request, ctx *model.Context) (err error) {
	note := noteDecoder(r)
	note.Create(ctx)
	response := model.MakeResponse(utility.ResponseSuccess, "note created", note)
	json.NewEncoder(w).Encode(response)
	return nil
}

func noteDecoder(r *http.Request) model.Note {
	// body, _ := ioutil.ReadAll(r.Body)
	// log.Println("req body ::: ", string(body))
	r.ParseForm()
	decoder := json.NewDecoder(r.Body)
	var t model.Note
	err := decoder.Decode(&t)
	if err != nil {
		panic(err)
	}
	return t
}
