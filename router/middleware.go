package router

import (
	"gitlab.com/vjopensrc/webserver_demo/model"
	"net/http"
)

// Normal handler
type Handler func(http.ResponseWriter, *http.Request, *model.Context) error

func (h Handler) ServeHTTP(w http.ResponseWriter, req *http.Request) {
	/*****create new context for each request.
	this implies that we are creating a new db connection for each request
	and closes that often. Is that fine ?
	****/
	// create new context
	ctx, err := model.NewContext()
	defer ctx.Close()
	if err != nil {
		return
	}

	// create user model
	// if err = setUser(w, req, ctx); err != nil {
	// 	return
	// }

	//run the handler and grab the error, and report it
	if err = h(w, req, ctx); err != nil {
		//http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
}
