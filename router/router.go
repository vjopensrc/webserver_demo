package router

import (
	"github.com/gorilla/mux"
	"gitlab.com/vjopensrc/webserver_demo/controller"
)

func InitRouter() *mux.Router {
	router := mux.NewRouter()
	//tickets
	router.Handle("/", Handler(controller.TicketListHandler)).Methods("GET")
	router.Handle("/tickets", Handler(controller.TicketCreateHandler)).Methods("POST")
	router.Handle("/tickets", Handler(controller.TicketListHandler)).Methods("GET")
	router.Handle("/tickets/{Ticketid}", Handler(controller.TicketDetailHandler)).Methods("GET")
	router.Handle("/notes", Handler(controller.NoteCreateHandler)).Methods("POST")
	return router
}
