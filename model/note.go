package model

import (
	"gitlab.com/vjopensrc/webserver_demo/utility"
	"log"
	"time"
)

type Note struct {
	Id          int64
	Ticketid    int64
	Name        string
	Description string
	Updated     int64
	Created     time.Time
}

func (note *Note) Create(ctx *Context) {
	note.Updated = utility.CurrentTime()
	note.Created = utility.CurrentTimeForDB()
	err := ctx.Dbmap.Insert(note)
	if err != nil {
		log.Println("New Note Create Error :: ", err)
	}
}

func Notes(ctx *Context, ticketid int64) []Note {
	var notes []Note
	_, err := ctx.Dbmap.Select(&notes, NotesQuery, ticketid)
	if err != nil {
		log.Println("Note List Error :: ", err)
	}
	return notes
}
