package model

type Attachment struct {
	Id      int64
	Name    string
	Size    int64
	Noteid  int64
	Updated int64
	Created int64
}
