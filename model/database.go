package model

import (
	"database/sql"
	"fmt"
	_ "github.com/lib/pq"
	"gitlab.com/vjopensrc/webserver_demo/utility"
	"gopkg.in/gorp.v1"
)

var dbmap *gorp.DbMap
var createTableFlag bool = false
var timelikedbformat string = "2006-01-02 15:04:05-07:00"

type Context struct {
	Dbmap *gorp.DbMap
}

func (c *Context) Close() {
	c.Dbmap.Db.Close()
}

func NewContext() (*Context, error) {
	dbmap, err := initGorp()
	utility.CheckError(err, "CREATE SQL CONTEXT")
	updateTableNameMap(dbmap)
	return &Context{
		Dbmap: dbmap,
	}, err
}

func initGorp() (*gorp.DbMap, error) {
	// connect to db using standard Go database/sql API
	// use whatever database/sql driver you wish
	db, err := sql.Open("postgres", utility.GetConfigInstance().GetString("database.sourcename"))
	utility.CheckError(err, "sql.Open failed")
	// construct a gorp DbMap
	dbmap := &gorp.DbMap{Db: db, Dialect: gorp.PostgresDialect{}}
	//defer dbmap.Db.Close()
	return dbmap, err
}

func CreateSchemaFirstTime() {
	dbmap, _ := initGorp()
	fmt.Println(":::::::::::::::::::::::::::::::::::::CreateSchemaFirstTime::::::::::::::::::::::::::::::::::::: ")
	fmt.Println("Notes to dev :::::::::: Please comment CreateSchemaFirstTime method from the server.go and restart the server before continue")
	if !createTableFlag {
		createTableFlag = true
		createschema(dbmap)
		insertSeedData(dbmap)
	}
	//addNewTables(dbmap)
}

func createschema(dbmap *gorp.DbMap) {

	trans, _ := dbmap.Begin()
	//members
	_, err := trans.Exec(CreateTicketQuery)
	utility.CheckError(err, "Create ticket table")
	//Index
	_, err = trans.Exec(CreateTicketIndex)
	utility.CheckError(err, "Ticket index")

	//tasks
	_, err = trans.Exec(CreateNoteQuery)
	utility.CheckError(err, "Create note table")
	//Index
	_, err = trans.Exec(CreateNoteIndex)
	utility.CheckError(err, "Note index")

	//Attachments
	_, err = trans.Exec(CreateAttachmentQuery)
	utility.CheckError(err, "Create attachment table")
	//Index
	_, err = trans.Exec(CreateAttachmentIndex)
	utility.CheckError(err, "Attachment index")

	//TimeZone
	_, err = trans.Exec("SET timezone = 'UTC'")
	utility.CheckError(err, "Set Timezone to UTC")

	//Commit
	trans.Commit()
}

func updateTableNameMap(dbmap *gorp.DbMap) {
	dbmap.AddTableWithName(Ticket{}, "tickets").SetKeys(true, "Id")
	dbmap.AddTableWithName(Note{}, "notes").SetKeys(true, "Id")
	dbmap.AddTableWithName(Attachment{}, "attachments").SetKeys(true, "Id")
}

func AddNewTables() {
	dbmap, _ := initGorp()

	trans, _ := dbmap.Begin()
	//Attachments
	_, err := trans.Exec(CreateAttachmentQuery)
	utility.CheckError(err, "Create attachments table")

	//Commit
	trans.Commit()
}

func insertSeedData(dbmap *gorp.DbMap) {

}

const (
	//Ticket
	CreateTicketQuery = "CREATE TABLE IF NOT EXISTS tickets(id bigserial NOT NULL,subject text,description text,updated bigint,created timestamptz, CONSTRAINT tickets_pkey PRIMARY KEY (id)) WITH (OIDS=FALSE);"
	CreateTicketIndex = "CREATE INDEX ticket_index ON tickets (updated);"

	//Task
	CreateNoteQuery = "CREATE TABLE IF NOT EXISTS notes (id bigserial NOT NULL,ticketid bigint REFERENCES tickets (id) ON DELETE CASCADE,name text,description text,updated bigint,created timestamptz,CONSTRAINT notes_pkey PRIMARY KEY (id)) WITH (OIDS=FALSE);"
	CreateNoteIndex = "CREATE INDEX note_index ON notes (ticketid);"

	//Attachments
	CreateAttachmentQuery = "CREATE TABLE IF NOT EXISTS attachments (id bigserial NOT NULL,name text,size bigint,noteid bigint REFERENCES notes (id) ON DELETE CASCADE,updated bigint,created bigint,CONSTRAINT attachments_pkey PRIMARY KEY (id)) WITH (OIDS=FALSE);"
	CreateAttachmentIndex = "CREATE INDEX attachment_index ON attachments (noteid);"
)

const (
	TicketsQuery      = "SELECT * from tickets"
	TicketDetailQuery = "SELECT * from tickets where id = $1"
	NotesQuery        = "SELECT * from notes where ticketid = $1"
)
