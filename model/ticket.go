package model

import (
	"gitlab.com/vjopensrc/webserver_demo/utility"
	"log"
	"time"
)

type Ticket struct {
	Id          int64
	Subject     string
	Description string
	Updated     int64
	Created     time.Time
}

func (ticket *Ticket) Create(ctx *Context) {
	ticket.Updated = utility.CurrentTime()
	ticket.Created = utility.CurrentTimeForDB()
	err := ctx.Dbmap.Insert(ticket)
	if err != nil {
		log.Println("New Ticket Create Error :: ", err)
	}
}

func Tickets(ctx *Context) []Ticket {
	var tickets []Ticket
	_, err := ctx.Dbmap.Select(&tickets, TicketsQuery)
	if err != nil {
		log.Println("Ticket List Error :: ", err)
	}
	return tickets
}

func TicketDetail(ctx *Context, id int64) Ticket {
	var ticket Ticket
	err := ctx.Dbmap.SelectOne(&ticket, TicketDetailQuery, id)
	if err != nil {
		log.Println("Ticket detail Error :: ", err)
	}
	return ticket
}
