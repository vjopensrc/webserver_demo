package main

import (
	"github.com/codegangsta/negroni"
	"github.com/rs/cors"
	//"gitlab.com/vjopensrc/webserver_demo/model"
	"gitlab.com/vjopensrc/webserver_demo/router"
	"gitlab.com/vjopensrc/webserver_demo/utility"
)

func main() {

	//model.CreateSchemaFirstTime()

	c := cors.New(cors.Options{
		AllowedOrigins: []string{"http://localhost:3000"},
	})

	n := negroni.Classic()
	n.Use(c)
	n.UseHandler(router.InitRouter())
	n.Run(":" + utility.GetConfigInstance().GetString("port"))
}
