package utility

import (
	"github.com/spf13/viper"
	"log"
	"sync"
)

var instance *viper.Viper
var once sync.Once

func GetConfigInstance() *viper.Viper {
	once.Do(func() {
		instance = viper.New()
		instance.SetConfigName("config")
		instance.AddConfigPath("./")
		err := instance.ReadInConfig()
		if err != nil {
			log.Println(err, "Error in reading config file")
		}
	})
	return instance
}

func CheckError(err error, msg string) bool {
	if err != nil {
		log.Panicln(msg, err)
		return true
	}
	return false
}

const (
	ResponseSuccess         = 1
	ResponseError           = 1000
	ResponsePermissionError = 5000
	ResponseAuthError       = 6000
)
