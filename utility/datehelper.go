package utility

import (
	"time"
)

func CurrentTime() int64 {
	return GetMilliSeconds(time.Now())
}

func GetMilliSeconds(now time.Time) int64 {
	return now.UTC().Unix() * 1000
}

func CurrentTimeForDB() time.Time {
	t := time.Now()
	t = time.Date(t.Year(), t.Month(), t.Day(), t.Hour(), t.Minute(), 0, 0, t.Location())
	return t
}
